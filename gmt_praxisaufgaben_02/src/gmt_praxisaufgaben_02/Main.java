package gmt_praxisaufgaben_02;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * The {@code Encode} class provides static methods for compressing
 * text streams with <i>Limpel-Yiv-Welch Algorithm</i>.
 *
 * @author Mykola Zakharchuk(3991624)
 * @author Zabiha Kehribar (3994553)
 */

public class Main {
	public static final int STD_ASCII = 255;
	public static final String EMPTY_STRING = "";

	@SuppressWarnings("static-access")
	public static void main(String[] args) {
		Map<String, Integer> dictionary = new LinkedHashMap<String, Integer>();
		fillDictionary(dictionary, STD_ASCII);
		LinkedList<String> printStream = new LinkedList<String>();

		
		TextHelfer textHelfer = new TextHelfer("src/text.txt", "src/ausgabe.txt");
		textHelfer.lese_text();

		// encode to LZW and print in console
		textHelfer.text = encodeToLZW(dictionary, textHelfer.text, printStream).toString();
		printTableToConsole(printStream);
		
		// print converted stream in console
		System.out.println(textHelfer.text);

		// save to file
		textHelfer.schreibe_text();
	}
	
	/** 
	 *  Compress incoming String using an <i>Limpel-Yiv-Welch Algorithm</i> algorithm
	 *  
	 *  @param dictionary Map of KEY = VALUE collection
	 *  @param string input String to compress
	 *  @param toPrint a sequential list of all table items
	 */
	public static ArrayList<Integer> encodeToLZW(Map<String, Integer> dictionary, String string,
			LinkedList<String> toPrint) {
		ArrayList<Character> iterable = stringToCharList(string);
		String firstChar = EMPTY_STRING;
		ArrayList<Integer> result = new ArrayList<Integer>();

		for (int i = 0; i < iterable.size(); i++) {
			toPrint.add(iterable.get(i) + "(" + dictionary.get(EMPTY_STRING + iterable.get(i)) + ")");

			String firstCharWithNextChar = firstChar + iterable.get(i);
			if (dictionary.containsKey(firstCharWithNextChar)) {
				toPrint.add(EMPTY_STRING);
				toPrint.add(EMPTY_STRING);
			}
			if (dictionary.containsKey(firstCharWithNextChar)) {
				if (firstChar.equals(firstCharWithNextChar)) {
					toPrint.add(EMPTY_STRING);
				}
				firstChar = firstCharWithNextChar;
			} else {
				toPrint.add(dictionary.get(firstChar) + " (" + firstChar + ")");
				result.add(dictionary.get(firstChar));
				dictionary.put(firstCharWithNextChar, dictionary.size() + 1);
				toPrint.add(firstCharWithNextChar + " (" + dictionary.size() + ")");
				firstChar = EMPTY_STRING + iterable.get(i);
			}
		}
		if (!firstChar.equals(EMPTY_STRING)) {
			result.add(dictionary.get(firstChar));
		}
		return result;
	}

	public static void printTableToConsole(LinkedList<String> printable) {
		for (int i = 0; i < printable.size(); i += 3) {
		}
		System.out.println("------------------------------------------------");
		System.out.printf("%10s %15s %15s", "Lese Zeich.", "Neuer Eintrag", "Ausgabe");
		System.out.println();
		System.out.println("------------------------------------------------");
		for (int i = 0; i < printable.size(); i += 3) {
			System.out.format("%10s %15s %15s", printable.get(i), printable.get(i + 2), printable.get(i + 1));
			System.out.println();
		}
		System.out.printf("%10s %15s %15s", "EOF", " ", printable.get(printable.size() - 3));
		System.out.println();
		System.out.println("-------------------------------------------------");
	}

	public static ArrayList<Character> stringToCharList(String stringToConvert) {
		ArrayList<Character> charList = new ArrayList<>();
		for (int i = 0; i < stringToConvert.length(); i++) {
			charList.add(stringToConvert.charAt(i));
		}
		return charList;
	}

	public static Map<String, Integer> fillDictionary(Map<String, Integer> dict, int dictionarySize) {
		for (int i = 0; i < dictionarySize; i++)
			dict.put(String.valueOf((char) i), i);
		return dict;
	}
}