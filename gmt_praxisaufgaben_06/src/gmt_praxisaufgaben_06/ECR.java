package gmt_praxisaufgaben_06;

import java.util.LinkedList;

import ij.IJ;
import ij.ImageJ;
import ij.ImagePlus;
import ij.gui.GenericDialog;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageConverter;
import ij.process.ImageProcessor;

public class ECR implements PlugInFilter {
	protected ImagePlus image;
	static final float CUT_INDEX = 0.5f;
	LinkedList<Integer> listOfCutFramesNumbers = new LinkedList<>();
	int amountOfCutFrames = 0;

	int sobelX[][] = { { 1, 0, -1 }, { 2, 0, -2 }, { 1, 0, -1 } };
	int sobelY[][] = { { 1, 2, 1 }, { 0, 0, 0 }, { -1, -2, -1 } };
	int pixelX;
	int pixelY;

	public double value;
	public String name;

	@Override
	public int setup(String arg, ImagePlus imp) {
		ImageConverter ic = new ImageConverter(imp);
		ic.convertToGray8();
		imp.updateAndDraw();
		image = imp;
		return DOES_ALL;
	}

	@Override
	public void run(ImageProcessor ip) {
		// applyFilter(ip);
		applySobel(image);
		getEdges(image);

		// draw signatures
		ip.drawString("Mykola Zakharchuk(3991624)", 10, 20);
		ip.drawString("Zabiha Kehribar (3994553)", 10, 40);

		if (showDialog()) {
			image.updateAndDraw();
		}
	}

	private void applySobel(ImagePlus imp) {
		for (int i = 1; i < imp.getImageStackSize(); i++) {
			ImageProcessor frame = imp.getImageStack().getProcessor(i);
			int width = frame.getWidth();
			int height = frame.getHeight();
			ImageProcessor copy = frame.duplicate();
			for (int x_Axe = 1; x_Axe < width - 2; x_Axe++) {
				for (int y_Axe = 1; y_Axe < height - 2; y_Axe++) {
					pixelX = (sobelX[0][0] * copy.getPixel(x_Axe - 1, y_Axe - 1)) + (sobelX[0][1] * copy.getPixel(x_Axe, y_Axe - 1))
							+ (sobelX[0][2] * copy.getPixel(x_Axe + 1, y_Axe - 1)) + (sobelX[1][0] * copy.getPixel(x_Axe - 1, y_Axe))
							+ (sobelX[1][1] * copy.getPixel(x_Axe, y_Axe)) + (sobelX[1][2] * copy.getPixel(x_Axe + 1, y_Axe))
							+ (sobelX[2][0] * copy.getPixel(x_Axe - 1, y_Axe + 1)) + (sobelX[2][1] * copy.getPixel(x_Axe, y_Axe + 1))
							+ (sobelX[2][2] * copy.getPixel(x_Axe + 1, y_Axe + 1));
					pixelY = (sobelY[0][0] * copy.getPixel(x_Axe - 1, y_Axe - 1)) + (sobelY[0][1] * copy.getPixel(x_Axe, y_Axe - 1))
							+ (sobelY[0][2] * copy.getPixel(x_Axe + 1, y_Axe - 1)) + (sobelY[1][0] * copy.getPixel(x_Axe - 1, y_Axe))
							+ (sobelY[1][1] * copy.getPixel(x_Axe, y_Axe)) + (sobelY[1][2] * copy.getPixel(x_Axe + 1, y_Axe))
							+ (sobelY[2][0] * copy.getPixel(x_Axe - 1, y_Axe + 1)) + (sobelY[2][1] * copy.getPixel(x_Axe, y_Axe + 1))
							+ (sobelX[2][2] * copy.getPixel(x_Axe + 1, y_Axe + 1));

					int value = (int) Math.sqrt((pixelX * pixelX) + (pixelY * pixelY));
					if (value < 0) {
						value = 0;
					}
					if (value > 255) {
						value = 255;
					}
					frame.putPixel(x_Axe, y_Axe, value);
				}
			}
			frame.convertToByteProcessor(true);
		}
	}
	
	private void getEdges(ImagePlus imp) {
		float frameIndex;
		for (int q = 1; q < imp.getImageStackSize() - 1; q++) {			
			int edgePixelsFirst = getNumberOfEdgePixels(imp, q);
			int edgePixelsSecond = getNumberOfEdgePixels(imp, q + 1);
			int eIn = getNumberOfEIn(imp, q);
			int eOut = getNumberOfEOut(imp, q);
			float ecpLeft = (float)eIn / edgePixelsFirst;
			float ecpRight = (float)eOut / edgePixelsSecond;
			if (ecpLeft > ecpRight) {
				frameIndex = ecpLeft;
			} else {
				frameIndex = ecpRight;
			}
			if (frameIndex > CUT_INDEX) {
				amountOfCutFrames++;
				listOfCutFramesNumbers.add(q);
			}
		}
	}

	private int getNumberOfEOut(ImagePlus imp, int q) {
		ImageProcessor image = imp.getImageStack().getProcessor(q);
		ImageProcessor image2 = imp.getImageStack().getProcessor(q + 1);
		int edgePixels = 0;
		for (int i = 0; i < image.getWidth(); i++) {
			for (int j = 0; j < image.getHeight(); j++) {
				int col = image.getPixel(i, j);
				int col2 = image2.getPixel(i, j);
				if (col == 255 && col2 != 255) {
					edgePixels++;
				} 
			}
		}
		return edgePixels;
	}
	
	private int getNumberOfEIn(ImagePlus imp, int q) {
		ImageProcessor image = imp.getImageStack().getProcessor(q);
		ImageProcessor image2 = imp.getImageStack().getProcessor(q + 1);
		int edgePixels = 0;
		for (int i = 0; i < image.getWidth(); i++) {
			for (int j = 0; j < image.getHeight(); j++) {
				int col = image.getPixel(i, j);
				int col2 = image2.getPixel(i, j);
				if (col != 255 && col2 == 255) {
					edgePixels++;
				} 
			}
		}
		return edgePixels;
	}
	
	private int getNumberOfEdgePixels(ImagePlus imp, int q) {
		ImageProcessor image = imp.getImageStack().getProcessor(q);
		int edgePixels = 0;
		for (int i = 0; i < imp.getWidth(); i++) {
			for (int j = 0; j < imp.getHeight(); j++) {
				int col = image.getPixel(i, j);
				if (col == 255) {
					edgePixels++;
				} 
			}
		}
		return edgePixels;
	}

	private boolean showDialog() {
		GenericDialog gd = new GenericDialog("ECR");
		gd.addMessage("Mykola Zakharchuk(3991624)" + " Zabiha Kehribar (3994553)");
		gd.addMessage("Cut Frames overall: " + amountOfCutFrames);
		gd.addMessage("Cut Frames: ");
		int shown = 0;
		for (Integer integer : listOfCutFramesNumbers) {
			gd.addMessage(integer.toString());
			shown++;
			if (shown > 7) {
				gd.addMessage("...");
				break;
			}
		}
		gd.showDialog();
		if (gd.wasCanceled())
			return false;
		return true;
	}

	public static void main(String[] args) {
		Class<?> classToEdit = ECR.class;
		String url = classToEdit.getResource("/" + classToEdit.getName().replace('.', '/') + ".class").toString();
		String pluginsDir = url.substring("file:".length(),
				url.length() - classToEdit.getName().length() - ".class".length());
		System.setProperty("plugins.dir", pluginsDir);

		// start ImageJ
		new ImageJ();

		// change a path to apply filter directly to the picture
		ImagePlus video = IJ.openImage("C:\\Users\\Nikolay\\Desktop\\tagesschau_intro.gif");
		video.show();

		// run the plugin
		IJ.runPlugIn(classToEdit.getName(), "");

	}
}
