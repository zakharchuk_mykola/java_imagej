package gmt_praxisaufgaben_05;


import java.util.LinkedList;
import java.util.TreeMap;

import ij.IJ;
import ij.ImageJ;
import ij.ImagePlus;
import ij.gui.GenericDialog;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;

public class Grey_Plugin implements PlugInFilter {
	protected ImagePlus image;

	private final static int ZERO = 0;
	private final static int UPPER = 255;
	private final static int LOWER = 0;
	private static TreeMap<Integer, LinkedList<Integer>> greySet = new TreeMap<>();
	private static int minGreyValue;
	private static int maxGreyValue;


	@Override
	public int setup(String arg, ImagePlus imp) {
		image = imp;
		return DOES_ALL;
	}

	@Override
	public void run(ImageProcessor ip) {
		// draw signatures
		ip.drawString("Mykola Zakharchuk(3991624)", 10, 20);
		ip.drawString("Zabiha Kehribar (3994553)", 10, 40);
		
		getGreyPalette(ip);
		minGreyValue = greySet.firstKey();
		maxGreyValue = greySet.lastKey();
		
		stretchMinMaxToBoundaries(greySet);
		skaleGrayValues(greySet, ip);
		applyFilter(ip, greySet);
	
		drawGreyValuesSet();
		
		if (showDialog()) {
			image.updateAndDraw();
		}
	}
	
	private static void stretchMinMaxToBoundaries(TreeMap<Integer,LinkedList<Integer>> greySet2) {
		LinkedList<Integer> max = greySet2.get(maxGreyValue);
		LinkedList<Integer> min = greySet2.get(minGreyValue);
		max.add(UPPER);
		min.add(LOWER);
		
		greySet2.put(maxGreyValue, max);
		greySet2.put(minGreyValue, min);
	}

	private static int stratchGreyValue(int value, int maxGreyValue, int minGreyValue) {
		if (value != UPPER && value != LOWER) {
			Double result = new Double(((double) value) / maxGreyValue * 255);
			return result.intValue();			
		}
		return value;
	}
	
	private static void skaleGrayValues(TreeMap<Integer,LinkedList<Integer>> greySet2, ImageProcessor ip) {
		for (Integer elem : greySet.keySet()) {
			LinkedList<Integer> greyValue = greySet.get(elem);
			int stratchedGreyValue = stratchGreyValue(elem, maxGreyValue, minGreyValue);
			greyValue.add(stratchedGreyValue);
			greySet.put(elem, greyValue);
		}
	}

	private static void applyFilter(ImageProcessor ip, TreeMap<Integer,LinkedList<Integer>> greySet) {
		for (int i = 0; i < ip.getWidth(); i++) {
			for (int j = 0; j < ip.getHeight(); j++) {
				int pixel = ip.getPixel(i, j);
				if (greySet.containsKey(pixel)) {				
					ip.putPixel(i, j, greySet.get(pixel).get(1));
				}
				
			}
		}
	}
	
	private static void drawGreyValuesSet() {
		for (Integer elem : greySet.keySet()) {
			System.out.print("Grey value: " + elem  + " Stratched grey value: " 
					+ greySet.get(elem).get(1) + " Amount of Pixels: " + greySet.get(elem).get(0));
			System.out.println();
		}
	}

	private void getGreyPalette(ImageProcessor ip) {
		for (int i = 0; i < ip.getWidth(); i++) {
			for (int j = 0; j < ip.getHeight(); j++) {
				int col = ip.getPixel(i, j);
				if (!greySet.containsKey(col)) {
					LinkedList<Integer> pair = new LinkedList<>();
					pair.add(ZERO);
					greySet.put(col, pair);	
				} else {
					LinkedList<Integer> pair = new LinkedList<>();
					pair.add(ZERO);
					LinkedList<Integer> newValue = greySet.get(col);
					newValue.set(0, newValue.get(0) + 1);
					greySet.put(col, newValue);
				}
			}
		}
	}


	private boolean showDialog() {
		GenericDialog gd = new GenericDialog("Process...");
		gd.showDialog();
		if (gd.wasCanceled())
			return false;
		return true;
	}


	public static void main(String[] args) {
		Class<?> classToEdit = Grey_Plugin.class;
		String url = classToEdit.getResource("/" + classToEdit.getName().replace('.', '/') + ".class").toString();
		String pluginsDir = url.substring("file:".length(), url.length() - classToEdit.getName().length() - ".class".length());
		System.setProperty("plugins.dir", pluginsDir);

		// start ImageJ
		new ImageJ();

		// change a path to apply filter directly to the picture
		ImagePlus image = IJ.openImage("C:\\Users\\Nikolay\\Desktop\\bilder\\oilwagon.jpg");
		image.show();

		// run the plugin
		IJ.runPlugIn(classToEdit.getName(), "");
	}
}
