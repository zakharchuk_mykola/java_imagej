package gmt_praxisaufgaben_05;



import java.util.TreeSet;

import ij.IJ;
import ij.ImageJ;
import ij.ImagePlus;
import ij.gui.GenericDialog;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;

public class Median_Filter implements PlugInFilter {
	protected ImagePlus image;

	TreeSet<Integer> sortedListOfGreyValues = new TreeSet<>();

	public double value;
	public String name;
	

	@Override
	public int setup(String arg, ImagePlus imp) {
		image = imp;
		return DOES_ALL;
	}

	@Override
	public void run(ImageProcessor ip) {
		applyFilter(ip);
		// draw signatures
		ip.drawString("Mykola Zakharchuk(3991624)", 10, 20);
		ip.drawString("Zabiha Kehribar (3994553)", 10, 40);
		

		if (showDialog()) {
			image.updateAndDraw();
		}
	}


	private void applyFilter(ImageProcessor ip) {
		for (int i = 0; i < ip.getWidth(); i++) {
			for (int j = 0; j < ip.getHeight(); j++) {
				try {
					int median = (ip.getPixel(i - 1, j - 1) + ip.getPixel(i, j - 1) + ip.getPixel(i + 1, j - 1)
					+ ip.getPixel(i - 1, j) + ip.getPixel(i, j) + ip.getPixel(i + 1, j)
					+ ip.getPixel(i - 1, j + 1) + ip.getPixel(i, j + 1) + ip.getPixel(i + 1, j + 1)) / 9;

					ip.putPixel(i, j, median);
				} catch (Exception e) {
				}
			
			}
		}
	}
	
	

	private boolean showDialog() {
		GenericDialog gd = new GenericDialog("Process...");
		gd.showDialog();
		if (gd.wasCanceled())
			return false;
		return true;
	}


	public static void main(String[] args) {
		Class<?> classToEdit = Median_Filter.class;
		String url = classToEdit.getResource("/" + classToEdit.getName().replace('.', '/') + ".class").toString();
		String pluginsDir = url.substring("file:".length(), url.length() - classToEdit.getName().length() - ".class".length());
		System.setProperty("plugins.dir", pluginsDir);

		// start ImageJ
		new ImageJ();

		// change a path to apply filter directly to the picture
		ImagePlus image = IJ.openImage("C:\\Users\\Nikolay\\Desktop\\bilder\\oilwagon_snp.jpg");
		image.show();

		// run the plugin
		IJ.runPlugIn(classToEdit.getName(), "");
	}
}
