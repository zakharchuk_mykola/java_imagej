package gmt_praxisaufgaben_1;


import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 *  The {@code Encode} class provides static methods for compressing
 *  and expanding text streams with <i>Run Length Algorithm</i>
 *  and <i>Huffman Code</i>.
 *
 *  @author Mykola Zakharchuk(3991624)
 *  @author Zabiha Kehribar (3994553)
 */

public class Main {

	@SuppressWarnings("static-access")
	public static void main(String[] args) {
		
		// Parse text
		TextHelfer textHelfer = new TextHelfer("src/text_1.txt", "src/ausgabe.txt");
		textHelfer.lese_text();
		System.out.println(textHelfer.text);
		
		
		
		/* Aufgabe 1 */
		// Encode: Run Length
//		textHelfer.text = encodeToRunLength(textHelfer.text);
		
		
		// Fill alphabet with values(Alphabet A)
		Map<String, String> alphabet = new LinkedHashMap<String, String>();
		alphabet.put("E", "01");
		alphabet.put("S", "001");
		alphabet.put("N", "0001");
		alphabet.put("R", "0000");
		alphabet.put("A", "111");
		alphabet.put("H", "100");
		alphabet.put("U", "101");
		alphabet.put("C", "110");
		
		/* Aufgabe 2 */
		// Encode/Decode: Huffman Encoding
		textHelfer.text = encodeToHuffman(textHelfer.text, alphabet);
//		System.out.println(decodeFromHuffman(textHelfer.text, alphabet));
		
		
		// Dump to file
		System.out.println(textHelfer.text); 
		textHelfer.schreibe_text();
		
}

	/**
	 *  Replace chars from alphabet with appropriate values
	 *  
	 *  @param input String to encode with Huffman Encoding
	 *  @param alphabet map of encoding generated from Huffman tree
	 * */
	public static String encodeToHuffman(String input, Map<String, String> alphabet) {
	    StringBuffer sb = new StringBuffer();
	    int prefixIndex = 0;
	    for (int i = prefixIndex + 1; i <= input.length(); i++) {
	        while (i <= input.length()) {
	        	String word = input.substring(prefixIndex, i);
	        	if (alphabet.containsKey(word)) {
					sb.append(alphabet.get(word));
					prefixIndex = i;
					break;
				} else {
					System.out.println("Char '" + word + "' is out of alphabet");
					prefixIndex = i;
					break;
				}
	        }
	    }
	    return sb.toString();
	}
	
	/**
	 *  Replace encoded stream with chars from alphabet
	 *  
	 *  @param input String to encode with Huffman Encoding
	 *  @param alphabet map of encoding generated from Huffman tree
	 * */
	public static String decodeFromHuffman(String input, Map<String, String> alphabet) {
	    StringBuffer sb = new StringBuffer();
	    int prefixIndex = 0;
	    for (int i = prefixIndex + 1; i <= input.length(); i++) {
	        while (i <= input.length()) {
	        	String word = input.substring(prefixIndex, i);
	        	if (alphabet.containsValue(word)) {
					sb.append(getKeyByValue(alphabet, word));
					prefixIndex = i;
					break;
				} else {
					i++;
				}
	        }
	    }
	    return sb.toString();
	}
	
	/**
	 * Return key by value from Map
	 * <i>Only works for one-to-one relation</i>
	 * */
	public static String getKeyByValue(Map<String, String> map, String value) {
		String key = null;
		for (Entry<String, String> entry : map.entrySet()) {
            if (entry.getValue().equals(value)) {
            	key = entry.getKey();
            }
        }
	    return key;
	}
	
	/** 
	 *  Replace a value that is repeated consecutively with a token
	 *  that consists of the value and a count of the number of
	 *  consecutive occurrences 
	 *  
	 *  @param input String to encode with Run Length algorithm*/
	public static String encodeToRunLength(String input) {
	    StringBuffer sb = new StringBuffer();
	    for (int i = 0; i < input.length(); i++) {
	        int steps = 1;
	        while (i + 1 < input.length() && compareTwoChars(input, i)) { 
	            steps++;
	            i++;
	        }
	        sb.append(input.charAt(i));
	        sb.append(steps);
	    }
	    return sb.toString();
	}

	/** 
	 *  Return <b>true</b> if neighbour chars equal
	 *  @param input String to encode wit Run Length algorithm
	 *  @param i index of char*/
	private static boolean compareTwoChars(String input, int i) {
		return input.charAt(i) == input.charAt(i + 1);
	}

}